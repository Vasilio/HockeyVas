import unreal_engine as ue
import random
import math

class Avatar:

    # this is called on game start
    def begin_play(self):
        self.speed = 10
        
        
    # this is called at every 'tick'    
    def tick(self, delta_time):
        
        location = self.uobject.get_actor_location()
        
        location.z += delta_time*self.speed
        
        self.uobject.set_actor_location(location)
        self.uobject.call("MoveRight")
        
    
    def write_line(self):
        ue.log_warning("This is called from BP")

		
		
class TestA:
	
	
	
	def begin_play(self):
	#begin
		self.speed = 2
		self.amplitude = 100
		
		self.init_location = self.uobject.get_actor_location()
		self.time_pass = random.randrange(100)
	#end
	
	
	def tick(self, delta_time):
	#begin
		self.time_pass += delta_time
		
		new_z = math.sin( self.time_pass * self.speed ) * self.amplitude
		
		self.uobject.set_actor_location( self.init_location.x, self.init_location.y, self.init_location.z + new_z )
	#end
		
		
		
		
