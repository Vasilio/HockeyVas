// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/NoExportTypes.h"
#include "myStaticLib.generated.h"


UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ETeamEnum : uint8
{
	ET_Red 	UMETA(DisplayName = "red"),
	ET_Blue	UMETA(DisplayName = "Blue")
};



UCLASS()
class HOCKEY_API UmyStaticLib : public UObject
{
	GENERATED_BODY()
	

	
};
