// Fill out your copyright notice in the Description page of Project Settings.

#include "Hockey.h"
#include "myStaticLib.h"
#include "UnrealNetwork.h"
#include "CAvatar.h"
#include "CPawn.h"


// Sets default values
//::AShooterPlayerController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)

ACPawn::ACPawn(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//create scene root
	USceneComponent * sroot = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("SceneRoot"));
	RootComponent = sroot;


	USceneComponent * dummy_camera = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("dummy_camera"));

	dummy_camera->SetupAttachment(RootComponent);
	dummy_camera->SetRelativeLocation(FVector(0, 0, 175.f));

	//create camera
	main_camera = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("Camera"));
	main_camera->SetupAttachment(dummy_camera);

	main_camera->SetRelativeLocation( FVector( 0.f, 0.f, 0) );
	main_camera->bUsePawnControlRotation = true;
	main_camera->bLockToHmd = true;

}



void ACPawn::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	DOREPLIFETIME(ACPawn, team);
}


// Called when the game starts or when spawned
void ACPawn::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("ACPawn begin play"));

	InitViewPoint();
}

// Called every frame
void ACPawn::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void ACPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

}


void  ACPawn::InitViewPoint() {
	for (TActorIterator<AViewPoint> ActorItr(GetWorld()); ActorItr; ++ActorItr)
	{
		view_point = *ActorItr;

		if (view_point->team == this->team)
			return;
	}
};


void ACPawn::BirdFlyView() {
	//if (!HasAuthority()) return;


	if (!view_point)
		this->InitViewPoint();

	if (view_point && main_camera && Controller) {

		TryDetachLastAvatar();

		FVector v = view_point->sroot->GetComponentLocation();

		SetActorLocation(v);

		v = -v;
		v.Normalize();

		Controller->SetControlRotation( v.Rotation());
	}
};


void ACPawn::TryDetachLastAvatar() {
	if (last_avatar) {

		if (last_avatar->HasPuck) return;

		last_avatar->CShowBody(true);
		last_avatar = NULL;
	}
};

/*
void ACPawn::InputTurn(float axis_value) {
	AddControllerYawInput(axis_value);

	if ( !UmyStaticLib::IsServer(GetWorld()) 
		RotateAvatar();
	else
		ServerRotateAvatar(Controller->GetControlRotation() );
}; */


void ACPawn::RotateAvatar(FRotator rot) {
	if (last_avatar)
		last_avatar->RotatePodium( rot );
};


void ACPawn::ClientSetPawn_Implementation(FVector new_location, FRotator new_rotation) {
	SetActorLocation(new_location);

	if (Controller)
		Controller->SetControlRotation(new_rotation);

	//SetActorRotation(new_rotation);
};


void ACPawn::LaunchPuck() {

	if (last_avatar) {
		last_avatar->LaunchPuck();
		TryDetachLastAvatar();
	}
	else
		UE_LOG(LogTemp, Error, TEXT("CPawn::LaunchPuck: last_avatar is NULL"));

};


void ACPawn::ControlAvatar(ACAvatar * avatar) {

	if (!avatar) return;

	TryDetachLastAvatar();

	if (avatar->team ==  UmyStaticLib::GetTeamByNetMode(GetWorld())) {
		avatar->CShowBody(false);

		SetActorLocation( avatar->PodiumLocation() );
		Controller->SetControlRotation(avatar->PodiumRotation());
		//main_camera->SetWorldRotation();
	}
	else
		ClientSetPawn(avatar->PodiumLocation(), avatar->PodiumRotation());

	//save last avatar
	last_avatar = avatar;

};