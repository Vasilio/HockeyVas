// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Hockey.h"
#include "CPuck.generated.h"

UCLASS(Blueprintable)
class HOCKEY_API ACPuck : public AActor
{
	GENERATED_UCLASS_BODY()
	
public:	
	void CLaunch(FVector force, FVector from_pos = FVector::ZeroVector);
	void RanomLaunch();


	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;


	UFUNCTION(BlueprintCallable, Category = MyFunctions)
	void StopMovement(bool new_visibility = false);

	UFUNCTION(BlueprintCallable, Category = MyFunctions)
	void ResetPosition(FVector new_pos);

	FVector GetPosition();


	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	EPuckState PuckState;
	
	

private:

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent * body;

	
	
};
