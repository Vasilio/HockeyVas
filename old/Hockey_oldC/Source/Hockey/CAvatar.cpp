// Fill out your copyright notice in the Description page of Project Settings.

#include "Hockey.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"
#include "myStaticLib.h"
#include "CAvatar.h"


// Sets default values
ACAvatar::ACAvatar(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bAllowTickOnDedicatedServer = true;


	local_distance = 0;
	HasPuck = false;

	podium_radius = 125.f;
}

// Called when the game starts or when spawned
void ACAvatar::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("Avatar begin play"));

	
}

/*
void ACAvatar::PostActorConstruction() {
	Super::PostActorConstruction();
};*/


void ACAvatar::PostInitializeComponents() {
	Super::PostInitializeComponents();

	//FString s = FString::Printf(TEXT("%s post init"), *GetName());

	//GEngine->AddOnScreenDebugMessage(-1, 20.f, FColor::Green, *s);

	InitComponents();
	InitDistanceAndSpeed();

	InitPuck();
};


void ACAvatar::PostNetInit() {
	Super::PostNetInit();

	
};


// Called every frame
void ACAvatar::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	LookAtPuck();
}


void ACAvatar::InitComponents() {

	TArray<UActorComponent*> components;
	GetComponents<UActorComponent>(components);



	for (int32 i = 0; i < components.Num(); i++) //Count is zero
	{
		UStaticMeshComponent* mesh = Cast<UStaticMeshComponent>(components[i]);

		if (mesh) {
			if (mesh->GetName().Equals("Body")) {
				body = mesh;
				//UE_LOG(LogTemp, Warning, TEXT("body found"));
				continue;
			}

			if (mesh->GetName().Equals("Hook")) {
				hook = mesh;
				//UE_LOG(LogTemp, Warning, TEXT("hook found"));
				continue;
			}

			if (mesh->GetName().Equals("Podium")) {
				podium = mesh;
				continue;
			}

			if (mesh->GetName().Equals("PuckAim")) {
				puck_aim = mesh;
				continue;
			}


			if (mesh->GetName().Equals("PuckCollider")) {
				collider = mesh;
				collider->OnComponentBeginOverlap.AddDynamic(this, &ACAvatar::OnOverlapBegin);

				//UE_LOG(LogTemp, Error, TEXT("collider init complete"));

				continue;
			}

		}


		//spline check
		if (!spline) {
			spline = Cast<USplineComponent>(components[i]);

			if (spline) {
				//UE_LOG(LogTemp, Warning, TEXT("spline found"));
				continue;
			}
		}

	}
};


void ACAvatar::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) 
{
	//UE_LOG(LogTemp, Error, TEXT("overlap event"));

	if (Cast<ACPuck>(OtherActor))
		CCatchPuck();
};


void ACAvatar::CShowBody(bool visible) {
	if (body)
		body->SetVisibility(visible);
	else 
		UE_LOG(LogTemp, Warning, TEXT("Cannot change Body visibility due to its NULL"));

	if (hook)
		hook->SetVisibility(visible);
	else
		UE_LOG(LogTemp, Warning, TEXT("Cannot change Hook visibility due to its NULL"));
}


void ACAvatar::InitPuck() {
	
	puck = UmyStaticLib::GetPuck(GetWorld());

	if (!puck) {
		UE_LOG(LogTemp, Warning, TEXT("Cannot find CPuck in world"));
	}
};


void ACAvatar::CCatchPuck() {
	if (!puck) {
		UE_LOG(LogTemp, Error, TEXT("CCatchPuck: Puck is NULL!!!!"));
		return;
	}

	if (HasPuck)
		return;
	else
		HasPuck = true;

	//serve Puck
	puck->StopMovement();
	puck_aim->SetVisibility(true);
	

	//serve Pawn
	ACPawn * pawn = UmyStaticLib::GetPawnByTeam( GetWorld(), this->team);

	if (pawn) {
		//UE_LOG(LogTemp, Error, TEXT("Pawn: %s"), *pawn->GetName())
		pawn->ControlAvatar(this);
	}
		

};


void ACAvatar::AjustPuckPosition() {
	if (!puck) {
		UE_LOG(LogTemp, Error, TEXT("AjustPuckPosition: Puck is NULL!!!!"));
		return;
	}

	
	if (HasPuck && podium) {
		FVector v = podium->GetComponentRotation().Vector();
		v.Normalize();

		v = podium->GetComponentLocation() + v*125.f + FVector(0, 0, 20.f);
	}	
};


void ACAvatar::LaunchPuck() {

	if (puck && HasPuck && podium && puck_aim) {
		//calculate force and launch
		FVector force = podium->GetComponentRotation().Vector();
		force.Normalize();

		
		puck->CLaunch( force * 30000.f, puck_aim->GetComponentLocation() );
		
		//reset flag
		puck_aim->SetVisibility(false);
		HasPuck = false;
	} else 
		UE_LOG(LogTemp, Error, TEXT("ACAvatar::LaunchPuck ELSE condition"));

};



void ACAvatar::LookAtPuck() {

	//dont look on to owning puck
	if (HasPuck || !HasAuthority()) return;

	//try to look
	if (podium && puck) {
		FVector v = puck->GetPosition() - podium->GetComponentLocation();

		FRotator rot = v.Rotation();

		podium->SetWorldRotation( FRotator(0.f, rot.Yaw, 0.f));
	} else
		UE_LOG(LogTemp, Error, TEXT("LookAtPuck: Podium or Puck NULL!!!!, %s"), *this->GetName());

};


void ACAvatar::RotatePodium(FRotator rot) {

	if (!HasPuck) return;
	if (!podium) return;

	//rotate only around Z
	podium->SetWorldRotation(FRotator(0.f, rot.Yaw, 0.f));
	
};


FVector ACAvatar::PodiumLocation() {

	if (podium)
		return podium->GetComponentLocation();
	else
		return FVector::ZeroVector;
};


FRotator ACAvatar::PodiumRotation() {
	if (podium)
		return podium->GetComponentRotation();
	else
		return FRotator::ZeroRotator;
};


void ACAvatar::InitDistanceAndSpeed() {

	if (!spline) {
		UE_LOG(LogTemp, Error, TEXT("InitDistanceAndSpeed: Spline is NULL!!!!"));
		return;
	}

	float koef = 150.f;

	if (ActorHasTag("GK"))
		koef = 50.f;
	

	MaxDistance = spline->GetSplineLength();
	speed = MaxDistance / koef;
};


void ACAvatar::MoveInDirection(float direction) {

	if (!spline) {
		UE_LOG(LogTemp, Error, TEXT("MoveInDirection: Spline is NULL!!!!"));
		return;
	}

	if (!podium) {
		UE_LOG(LogTemp, Error, TEXT("MoveInDirection: Podium is NULL!!!!"));
		return;
	}


	//dont move if owning puck
	if (HasPuck) return;


	local_distance = FMath::Clamp(local_distance + speed*direction, 0.f, MaxDistance);

	FVector v = spline->GetLocationAtDistanceAlongSpline(local_distance, ESplineCoordinateSpace::Local);

	podium->SetRelativeLocation(v);
};

