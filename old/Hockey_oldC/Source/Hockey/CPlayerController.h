// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "myStaticLib.h"
#include "CAvatar.h"
#include "CPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class HOCKEY_API ACPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ETeamEnum team;

	UFUNCTION(BlueprintCallable, Category = MyFunctions)
	void OnConnect();

private:
	
	TArray<ACAvatar*> field_players;
	ACAvatar * goalkeeper;


	void InitTeam();
	
	bool ServerControlled();
	
	
	void CreatePawn();


	virtual void SetupInputComponent() override;

	void OnVerticalAxis(float axis_value);
	void OnHorizontalAxis(float axis_value);
	

	void OnFire();
	void OnFire2();

	void OnInputLookUp(float axis_value);
	void OnInputTurn(float axis_value);


	//move filed players
	void MoveFieldPlayers(float direction);

	UFUNCTION(Server, unreliable, WithValidation)
	void ServerMoveFieldPlayers(float direction);
	virtual bool ServerMoveFieldPlayers_Validate(float direction);
	virtual void ServerMoveFieldPlayers_Implementation(float direction);


	//move filed players
	void MoveGoalkeeper(float direction);

	UFUNCTION(Server, unreliable, WithValidation)
	void ServerMoveGoalkeeper(float direction);
	virtual bool ServerMoveGoalkeeper_Validate(float direction);
	virtual void ServerMoveGoalkeeper_Implementation(float direction);


	void SpawnOnAvatar(ACAvatar * avatar);

	void LaunchPuck();

	UFUNCTION(Server, unreliable, WithValidation)
	void ServerLaunchPuck();
	virtual bool ServerLaunchPuck_Validate();
	virtual void ServerLaunchPuck_Implementation();

	UFUNCTION(Server, unreliable, WithValidation)
	void ServerRotateAvatar(FRotator rot);
	virtual bool ServerRotateAvatar_Validate(FRotator rot);
	virtual void ServerRotateAvatar_Implementation(FRotator rot);

	void RandomLaunchPuck();

	
};
