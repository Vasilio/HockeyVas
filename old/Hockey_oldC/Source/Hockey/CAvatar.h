// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hockey.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/SplineComponent.h"
#include "CPuck.h"
#include "CAvatar.generated.h"


UCLASS(Blueprintable)
class HOCKEY_API ACAvatar : public AActor
{
	GENERATED_UCLASS_BODY()
	
public:	
	// Sets default values for this actor's properties
	//ACAvatar();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	//virtual void PostActorConstruction() override;


	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = MyFunctions)
	void CShowBody(bool visible);


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ETeamEnum team;

	UFUNCTION(BlueprintCallable, Category = MyFunctions)
	void CCatchPuck();

	UFUNCTION(BlueprintCallable, Category = MyFunctions)
	void MoveInDirection(float direction);

	void RotatePodium(FRotator rot);

	void LaunchPuck();

	bool HasPuck;

	FVector PodiumLocation();
	FRotator PodiumRotation();


private:

	UPROPERTY()
	ACPuck * puck;

	UStaticMeshComponent * body;
	UStaticMeshComponent * hook;
	UStaticMeshComponent * podium;
	UStaticMeshComponent * collider;
	UStaticMeshComponent * puck_aim;
	USplineComponent * spline;

	

	float podium_radius;
	float local_distance;
	float MaxDistance;
	float speed;
	

	virtual void PostInitializeComponents() override;
	virtual void PostNetInit() override;

	void InitComponents();
	void InitDistanceAndSpeed();

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	//puck
	void InitPuck();
	void AjustPuckPosition();
	

	void LookAtPuck();
	
};
