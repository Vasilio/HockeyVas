// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "UObject/NoExportTypes.h"
#include "Hockey.h"
#include "CPuck.h"
#include "CAvatar.h"
#include "CPawn.h"
#include "myStaticLib.generated.h"






UCLASS()
class HOCKEY_API UmyStaticLib : public UObject
{
	GENERATED_BODY()
	
public:
	static ACPuck * GetPuck(UWorld * world);
	static ACAvatar * GetAvatar(UWorld * world, FName tag, ETeamEnum team);
	static ACPawn * GetPawnByTeam(UWorld * world, ETeamEnum team);

	static bool IsServer(UWorld * world);
	static ETeamEnum GetTeamByNetMode(UWorld * world);
	
};
