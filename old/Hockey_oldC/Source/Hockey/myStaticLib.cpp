// Fill out your copyright notice in the Description page of Project Settings.

#include "Hockey.h"
#include "EngineUtils.h"
#include "CPuck.h"

#include "myStaticLib.h"




ACPuck * UmyStaticLib::GetPuck(UWorld * world) {

		
	for (TActorIterator<ACPuck> ActorItr(world); ActorItr; ++ActorItr)
	{
		return *ActorItr;
	}

	return NULL;
	
}; 


ACAvatar * UmyStaticLib::GetAvatar(UWorld * world, FName tag, ETeamEnum team) {
	for (TActorIterator<ACAvatar> ActorItr(world); ActorItr; ++ActorItr)
	{
		ACAvatar * avatar = *ActorItr;

		if (avatar->ActorHasTag(tag) && (avatar->team == team))
			return avatar;
	}

	return NULL;
};


ACPawn *  UmyStaticLib::GetPawnByTeam(UWorld * world, ETeamEnum team) {
	for (TActorIterator<ACPawn> ActorItr(world); ActorItr; ++ActorItr)
	{
		ACPawn * pawn = *ActorItr;

		if (pawn->team == team)
			return pawn;
	}

	return NULL;
};


bool UmyStaticLib::IsServer(UWorld * world) {

	ENetMode nmode = world->GetNetMode();

	return ((nmode == ENetMode::NM_ListenServer) || (nmode == ENetMode::NM_DedicatedServer));
};


ETeamEnum UmyStaticLib::GetTeamByNetMode(UWorld * world) {

	if (IsServer(world))
		return ETeamEnum::ET_Red;
	else
		return ETeamEnum::ET_Blue;

};