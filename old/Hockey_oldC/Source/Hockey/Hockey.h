// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"




UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class ETeamEnum : uint8
{
	ET_Red 	UMETA(DisplayName = "Red"),
	ET_Blue	UMETA(DisplayName = "Blue")
};


UENUM(BlueprintType)		//"BlueprintType" is essential to include
enum class EPuckState : uint8
{
	Ready 	UMETA(DisplayName = "Ready"),
	InGate	UMETA(DisplayName = "InGate"),
	OnField 	UMETA(DisplayName = "OnField")
};