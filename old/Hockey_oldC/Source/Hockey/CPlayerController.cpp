// Fill out your copyright notice in the Description page of Project Settings.

#include "Hockey.h"
#include "CPawn.h"
#include "myStaticLib.h"
#include "CPlayerController.h"



void ACPlayerController::SetupInputComponent() {
	Super::SetupInputComponent();

	if (InputComponent) {
		//UE_LOG(LogTemp, Error, TEXT("SetupInputComponent"));
		InputComponent->BindAxis("MoveForward", this, &ACPlayerController::OnVerticalAxis);
		InputComponent->BindAxis("KeeperMove", this, &ACPlayerController::OnHorizontalAxis);

		

		InputComponent->BindAxis("LookUp", this, &ACPlayerController::OnInputLookUp);
		InputComponent->BindAxis("Turn", this, &ACPlayerController::OnInputTurn);

		InputComponent->BindAction("Fire", IE_Pressed, this, &ACPlayerController::OnFire);
		InputComponent->BindAction("Fire2", IE_Pressed, this, &ACPlayerController::OnFire2);

		InputComponent->BindAction("temp_action", IE_Pressed, this, &ACPlayerController::RandomLaunchPuck);
	}

};


void ACPlayerController::RandomLaunchPuck() {

	if (!HasAuthority()) return;


	ACPuck * puck = UmyStaticLib::GetPuck(GetWorld());
	if (puck)
		puck->RanomLaunch();
};


void ACPlayerController::OnVerticalAxis(float axis_value) {
	
	if (axis_value != 0) {
		//UE_LOG(LogTemp, Error, TEXT("OnVerticalAxis"));

		if (HasAuthority()) {
			MoveFieldPlayers(axis_value);
			//UE_LOG(LogTemp, Error, TEXT("OnVerticalAxis AUTH"));
		}
		else {
			ServerMoveFieldPlayers(axis_value);
			//UE_LOG(LogTemp, Error, TEXT("OnVerticalAxis no AUTH"));
		}
	}
};


void ACPlayerController::OnHorizontalAxis(float axis_value) {
	if (axis_value != 0) {
		if (HasAuthority()) {
			MoveGoalkeeper(axis_value);
		}
		else {
			ServerMoveGoalkeeper(axis_value);
		}
	}
};


void ACPlayerController::OnFire() {
	if (HasAuthority())
		LaunchPuck();
	else
		ServerLaunchPuck();
};


void ACPlayerController::OnFire2() {
	ACPawn * pawn = Cast<ACPawn>(GetPawn());

	if (pawn)
		pawn->BirdFlyView();

};


void  ACPlayerController::OnInputLookUp(float axis_value) {
	this->AddPitchInput(axis_value);
		//AddControllerPitchInput(axis_value);
};


void  ACPlayerController::OnInputTurn(float axis_value) {
	this->AddYawInput(axis_value);

	ACPawn * pawn = Cast<ACPawn>(GetPawn());

	if (pawn) {
		if (!HasAuthority())
			ServerRotateAvatar(GetControlRotation());

		pawn->RotateAvatar( GetControlRotation() );
	}
		
};


bool ACPlayerController::ServerRotateAvatar_Validate(FRotator rot) {
	return true;
};

void ACPlayerController::ServerRotateAvatar_Implementation(FRotator rot) {
	ACPawn * pawn = Cast<ACPawn>(GetPawn());

	if (pawn) {
		pawn->RotateAvatar(rot);
	}
};



void ACPlayerController::InitTeam() {
	//init team
	//team = UmyStaticLib::GetTeamByNetMode(GetWorld());
	//FString mode_name = UmyStaticLib::IsServer(GetWorld()) ? TEXT("server") : TEXT("client");
	//FString mode_name = UmyStaticLib::IsServer(GetWorld()) ? TEXT("server") : TEXT("client");


	//FString s = FString::Printf(TEXT("Controller: %s, mode: %s"),  *GetName(), *mode_name);

	//GEngine->AddOnScreenDebugMessage(-1, 20.f, FColor::Cyan, *s);

	
	if (IsLocalController())
		team = ETeamEnum::ET_Red;
	else
		team = ETeamEnum::ET_Blue; 

	//init players
	for (TActorIterator<ACAvatar> ActorItr( GetWorld() ); ActorItr; ++ActorItr)
	{
		ACAvatar * avatar = *ActorItr;


		if (avatar->team == team) {
			if (avatar->ActorHasTag(FName("GK")))
				this->goalkeeper = avatar;
			else
				field_players.Add(avatar);
		}
			
	}
};


void ACPlayerController::MoveFieldPlayers(float direction) {
	for (ACAvatar* player : field_players)
	{
		player->MoveInDirection(direction);
	}
};


bool ACPlayerController::ServerMoveFieldPlayers_Validate(float direction) {
	return true;
};

void ACPlayerController::ServerMoveFieldPlayers_Implementation(float direction) {
	MoveFieldPlayers(direction);
}



void ACPlayerController::MoveGoalkeeper(float direction) {
	if (this->goalkeeper)
		goalkeeper->MoveInDirection(direction);
};


bool ACPlayerController::ServerMoveGoalkeeper_Validate(float direction) {
	return true;
};


void ACPlayerController::ServerMoveGoalkeeper_Implementation(float direction) {
	MoveGoalkeeper(direction);
}



void  ACPlayerController::LaunchPuck() {

	ACPawn * pawn = Cast<ACPawn>(GetPawn());

	if (pawn)
		pawn->LaunchPuck();
	else
		UE_LOG(LogTemp, Error, TEXT("ACPlayerController::LaunchPuck: my_pawn is NULL"));
};


bool ACPlayerController::ServerLaunchPuck_Validate() {
	return true;
};


void ACPlayerController::ServerLaunchPuck_Implementation() {
	LaunchPuck();
};


bool ACPlayerController::ServerControlled() {
	return IsLocalController();
};


void ACPlayerController::OnConnect() {

	FString smode = (GetWorld()->GetNetMode() == ENetMode::NM_ListenServer) || (GetWorld()->GetNetMode() == ENetMode::NM_Standalone) ? "server" : "client";

	

	GEngine->AddOnScreenDebugMessage(-1, 20.f, FColor::Green, *smode);

	InitTeam();

	CreatePawn();
	//SpawnOnAvatar( UmyStaticLib::GetAvatar(GetWorld(), FName("CF"), this->team) );
};


void ACPlayerController::SpawnOnAvatar(ACAvatar * avatar) {
	ACPawn * my_pawn = Cast<ACPawn>(GetPawn());

	if (my_pawn && avatar) {

		my_pawn->ControlAvatar(avatar);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("SpawnOnAvatar: avatar or pawn is missed"));
	}
};


void ACPlayerController::CreatePawn() {
	if (!HasAuthority()) return;

	if (GetPawn()) return; //pawn exists


	//spawn actor of pawn
	//prepare spawn params
	FActorSpawnParameters spawnParams; //struct

	spawnParams.Owner = this;
	spawnParams.Instigator = Instigator;

	//spawn Pawn
	ACPawn * my_pawn = GetWorld()->SpawnActor<ACPawn>(ACPawn::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator, spawnParams);

	if (my_pawn) {
		//replication
		my_pawn->team = this->team;
		my_pawn->SetReplicates(true);

		//possess
		Possess(my_pawn);

		
		my_pawn->BirdFlyView();
	} else 
		UE_LOG(LogTemp, Error, TEXT("CreatePawn: pawn is NULL"));

	
};

