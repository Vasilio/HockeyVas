// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hockey.h"
#include "GameFramework/Actor.h"
#include "ViewPoint.generated.h"

UCLASS()
class HOCKEY_API AViewPoint : public AActor
{
	GENERATED_UCLASS_BODY()
	
public:	
	

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ETeamEnum team;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USceneComponent * sroot;
	

	
};
