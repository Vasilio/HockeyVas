// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Hockey.h"
#include "GameFramework/Pawn.h"
#include "CAvatar.h"
#include "ViewPoint.h"
#include "CPawn.generated.h"



UCLASS()
class HOCKEY_API ACPawn : public APawn
{
	GENERATED_UCLASS_BODY()

public:
	// Sets default values for this pawn's properties
	

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	void ControlAvatar(ACAvatar * avatar);
	void RotateAvatar(FRotator rot);

	UPROPERTY(Replicated, VisibleAnywhere)
	ETeamEnum team;

	ACAvatar * last_avatar;

	void LaunchPuck();

	void BirdFlyView();
	void InitViewPoint();

private:

	UFUNCTION(Client, reliable)
	void ClientSetPawn(FVector new_location, FRotator new_rotation);
	

	UCameraComponent * main_camera;
	

	void TryDetachLastAvatar();

	
	AViewPoint * view_point;

};
