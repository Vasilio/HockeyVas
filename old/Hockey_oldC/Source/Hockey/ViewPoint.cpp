// Fill out your copyright notice in the Description page of Project Settings.

#include "Hockey.h"
#include "ViewPoint.h"

AViewPoint::AViewPoint(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	sroot = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("SceneRoot"));
	RootComponent = sroot;

	sroot->SetMobility(EComponentMobility::Movable);
}