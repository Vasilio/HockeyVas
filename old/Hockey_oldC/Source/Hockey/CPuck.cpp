// Fill out your copyright notice in the Description page of Project Settings.

#include "Hockey.h"
#include "CPuck.h"


// Sets default values
ACPuck::ACPuck(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {
	PrimaryActorTick.bCanEverTick = true;

	
	USceneComponent * sroot = ObjectInitializer.CreateDefaultSubobject<USceneComponent>(this, TEXT("SceneRoot"));
	sroot->SetRelativeLocation(FVector::ZeroVector);

	RootComponent = sroot;

	body = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("Body"));

	//RootComponent = body;

	body->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> Asset(TEXT("/Engine/BasicShapes/Cylinder.Cylinder"));
	static ConstructorHelpers::FObjectFinder <UMaterial>Material(TEXT("Material'/Game/Materials/M_Puck.M_Puck'"));

	if (Asset.Succeeded() && Material.Succeeded()) {
		body->SetStaticMesh(Asset.Object);

		body->SetRelativeLocation(FVector(0, 0, .1f));
		body->SetWorldScale3D( FVector(0.5f, 0.5f, 0.2f) );

		body->SetMaterial(0, Material.Object);

		body->SetSimulatePhysics(true);
	}


	PuckState = EPuckState::Ready;
}


void ACPuck::BeginPlay() {
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("ACPuck begin play"));
};


void ACPuck::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ACPuck::StopMovement(bool new_visibility) {

	if (!HasAuthority()) return;

	if (!body) {
		UE_LOG(LogTemp, Warning, TEXT("Puck body is NULL"));
		return;
	}

	body->SetAllPhysicsLinearVelocity(FVector::ZeroVector);
	body->SetAllPhysicsRotation(FQuat::Identity);
	body->SetVisibility(new_visibility);
};


void ACPuck::ResetPosition(FVector new_pos) {
	if (!HasAuthority()) return;

	if (body) {
		body->SetAllPhysicsLinearVelocity(FVector::ZeroVector);
		body->SetAllPhysicsPosition(new_pos);
		body->SetAllPhysicsRotation(  FQuat::Identity );
	}

};


FVector ACPuck::GetPosition() {

	if (body) {
		return body->GetComponentLocation();
	}
	else
		return FVector::ZeroVector;
};


void ACPuck::CLaunch(FVector force, FVector from_pos) {
	if (!HasAuthority()) return;

	if (!body) {
		UE_LOG(LogTemp, Warning, TEXT("Puck body is NULL"));
		return;
	}



	if (!body->IsVisible())
		body->SetVisibility(true);

	if (!from_pos.Equals(FVector::ZeroVector))
		body->SetAllPhysicsPosition(from_pos);

	//body->SetSimulatePhysics(true);
	body->AddForce(force);

	PuckState = EPuckState::OnField;
};


void ACPuck::RanomLaunch() {

	float alpha = FMath::RandRange(0.f, 360.f);

	FVector v = FRotator(0.f, alpha, 0.f).Vector();
	v.Normalize();

	float force = 30000.f;

	CLaunch(v*force);
};